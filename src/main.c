/******************************************************************************
 * @ file  main.c
 *
 * @brief
 *  Template project for easy "drag-n-drop" implementation.
 *
 * @details
 *  This template project already possesses in its configuration the most
 *      important include directories, such as Timer A, UART, I2C, SPI, etc..
 *      These are custom libraries, many of which are not already made, so this
 *      isn't a final project, but one which will grow in the future. The
 *      purpose is to implement an API-like library, like "driverlib" for the
 *      TIVA series (e.g. TM$C123GH6PM). It's important that these project(s)
 *      reside inside the same Workspace as the Libraries, as each library is
 *      planned as an apart project, so their content folders should not be
 *      misplaced, removed or excluded from their actual directories. It is also
 *      important to define the Argument variables for each library. One must go
 *      to "Tools>Configure Custom Argument Variables" and add the corresponding
 *      ones to the project, or all the libraries for easier implementation.
 *
 *  Created on: 25/10/2018
 *      Author: AntoMota
 ******************************************************************************/

#include "msp430g2553.h"
#include "uart_func.h"
#include "msp430_conf.h"
#include "msp430_timer_func.h"
#include <stdint.h>

void SwapChars(uint8_t *a, uint8_t *b);

volatile uint8_t Data;

void main(void)
{
    /* uint8_t bufferghini[20]; */
    /* uint8_t * p_buff; */
    uint8_t x1 = '0';
    uint8_t x2 = '2';
    
    msp430_conf();
    
    while(1)
    {
        __bis_SR_register(LPM0_bits + GIE); // Enter LPM0, interrupts enabled
        /* (uint8_t *)bufferghini = Data; */
       switch(Data)
       {
           case 'S':
           {
               SwapChars(&x1,&x2);     // Pass the pointers to variables x1 and x2 to the function
               uart_send_byte(&x1);
               uart_send_byte(&x2);
               uart_send_array("\r\n", 2);
               P1OUT &= ~BIT4;            // Set P1.0
               }
               break;
               default:
           {
               uart_send_array("Unknown Command: ", 17);
               uart_send_byte( (uint8_t *)&Data );
               uart_send_array("\r\n", 2);
           }
           break;
       }
        des_in_rx();
        timer_wait_def(10);
        P1OUT ^= 0x01;
        timer_wait_def(10);
        P1OUT ^= 0x01;
        timer_wait_def(10);
        P1OUT ^= 0x01;
        P1OUT ^= BIT0;
        hab_in_rx();
        
    }
}


#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
  Data = UCA0RXBUF;
  P1OUT ^= BIT0;
  wake(LPM0_bits);
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
  /* P1OUT ^= 0x01;                            // Toggle P1.0 */
  CCR0 += 65535;                            // Add Offset to CCR0
  /* CCR0 += 60000; */
  wake(LPM0_bits);
}

void SwapChars(uint8_t *a, uint8_t *b)
{
    uint8_t c = 0;      // initialise temporary variable c
    P1OUT |= BIT6;            // Set P1.0
    c = *a;      // copy the value in memory location a in variable c
    *a = *b;     // copy the value stored in memory location b into memory location a
    *b = c;      // copy the value temporarily stored in c into memory location b
}
